﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DiscountSystem
{
    enum RuleType
    {
        None,
        /* get N items of the same type for the price of X */
        itemsForPrice,
        /* buy item X, get another Y items of type X for free */
        buyXGetYFree,
        /* get X items for the price of Y items*/
        xForPriceY,
    }
  
    struct Rule<T, U>
    {
        public RuleType ruleType;

        public T FirstValue { get; set; }
        public U SecondValue { get; set; }
    }

    /// <summary>
    /// The main Discount System class.
    /// Contains all the methods to create a basic discount system
    /// </summary>
    public class UnidaysDiscountChallenge
    {
        public Dictionary<string, string> DiscountRules { get; set; }

        public Dictionary<string, decimal> PricingRules { get; set; }
        public List<string> Basket { get; set; }

        // Main constructor
        public UnidaysDiscountChallenge(List<(string ItemName, decimal Price, string DiscountRule)> pricingRules)
        {
            DiscountRules = new Dictionary<string, string>();
            PricingRules = new Dictionary<string, decimal>();
            Basket = new List<string>();
            // Populate discount and pricing rules
            foreach ((string ItemName, decimal Price, string DiscountRule) in pricingRules)
            {
                DiscountRules.Add(ItemName, DiscountRule); 
                PricingRules.Add(ItemName,Price);
            }
        }
        /// <summary>
        /// Adds an item to the basket
        /// </summary>
        /// <param name="newItem">Item to be added</param>
        public void AddToBasket(string newItem)
        {
            if (PricingRules.ContainsKey(newItem))
            {
                Basket.Add(newItem);
            }
            else
            {
                Console.WriteLine($"Error: invalid item name ({newItem}). Please check the pricing rules");
            }
        }
        /// <summary>
        /// Retrieve the numerical values from a string
        /// </summary>
        /// <param name="rule">String to get the numerical values from</param>
        /// <returns>The retrieved numerical values in an array of strings</returns>
        private string[] RetrieveNumericalValues(string rule)
        {
            // RegEx to match floating point numbers and integers
            Regex numericalValRule = new Regex(@"\d+\.\d{2}|\d+", RegexOptions.Compiled);

            var numericalValues = numericalValRule.Matches(rule)
                .Cast<Match>()
                .Select(m => m.Value)
                .ToArray();

            return numericalValues;
        }
        /// <summary>
        /// Calculates the discount of all the items.
        /// </summary>
        /// <param name="itemQuantity">Item types mapped to their quantities</param>
        /// <returns>The calculated discount</returns>
        private decimal CalculateDiscount(Dictionary<string, int> itemQuantity)
        {
            decimal discount = 0.0m;
            // Iterate through the item types in the basket 
            foreach (string key in itemQuantity.Keys)
            {
                var rule = ParseRule(DiscountRules[key]);
                
                try
                {
                    Rule<int, int> tmp = (Rule<int, int>)rule;

                    switch (tmp.ruleType)
                    {
                        case RuleType.None: break;
                        // Buy X items get Y for free
                        // FirstValue: X
                        // SecondValue: Y 
                        case RuleType.buyXGetYFree:
                            int boughtItems = itemQuantity[key];
                            int freeItems = boughtItems / (tmp.FirstValue + tmp.SecondValue);

                            discount += freeItems * PricingRules[key];

                            break;
                        // Get X items for the price of Y
                        // FirstValue: X
                        // SecondValue: Y
                        case RuleType.xForPriceY:
                            decimal discountPriceItems = itemQuantity[key] / tmp.FirstValue;
                            discount += discountPriceItems * PricingRules[key];

                            break;
                    }
                }
                catch
                {
                    // The only rule left is: Get X items for £Y
                    // FirstValue: X
                    // SecondValue: Y
                    Rule<int, decimal> tmp = (Rule<int, decimal>)rule;

                    int discountPriceItems = itemQuantity[key] / tmp.FirstValue;
                    
                    discount += discountPriceItems * (PricingRules[key] * tmp.FirstValue - tmp.SecondValue);
                }
            
            }

            return discount;
        }
        /// <summary>
        /// Calculates the price of all the items in the basket
        /// </summary>
        /// <returns>The total price of the items and the delivery cost</returns>
        public (decimal Total, decimal DeliveryCharge) CalculateTotalPrice()
        {
            decimal total = 0.0m;
            decimal discount = 0.0m;

            decimal delivery = 7.00m;

            Dictionary<string, int> itemQuantity = new Dictionary<string, int>();
            // Count the items in the basket and add their cost to the total price
            foreach (string item in Basket)
            {
                if (itemQuantity.ContainsKey(item) == false)
                {
                    itemQuantity[item] = 1;
                }
                else
                {
                    ++itemQuantity[item];
                }

                total += PricingRules[item];
            }

            discount = CalculateDiscount(itemQuantity);
            // Apply the discounts
            total -= discount;

            if (total == 0.0m || total >= 50.0m)
            {
                delivery = 0.0m;
            }

            return (total, delivery);
        }
        /// <summary>
        /// Parses a discount rule based on the pattern match
        /// </summary>
        /// <param name="rule">Rule's text to be matched against patterns</param>
        /// <returns>Parsed rule</returns>
        private object ParseRule(string rule)
        {
            Regex noneRule = new Regex(@"None", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            // RegEx rule to match "X items for £Y"
            Regex itemsForPriceRule = new Regex(@"\d+ for .\d+\.\d{2}", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            // RegEx rule to match "Buy X get Y free"
            Regex buyXGetYRule = new Regex(@"Buy \d+ get \d+ free", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            // RegEx rule to match "X for the price of Y"
            Regex xForPriceY = new Regex(@"\d+ for the price of \d+", RegexOptions.Compiled | RegexOptions.IgnoreCase);

            if (noneRule.Matches(rule).Count > 0)
            {
                Rule<int, int> newRule = new Rule<int, int>
                {
                    ruleType = RuleType.None
                };

                return newRule;
            }

            string[] numericalValues = RetrieveNumericalValues(rule);
            
            int.TryParse(numericalValues[0], out int FirstValue);
            // Match rule against each RegEx pattern to find the rule type
            if (int.TryParse(numericalValues[1], out int secondValueI))
            { 
                Rule<int, int> newRule = new Rule<int, int>
                {
                    FirstValue = FirstValue,
                    SecondValue = secondValueI
                };

                if (buyXGetYRule.Matches(rule).Count > 0)
                {    
                    newRule.ruleType = RuleType.buyXGetYFree;
                }

                if (xForPriceY.Matches(rule).Count > 0)
                {
                    newRule.ruleType = RuleType.xForPriceY;
                }

                return newRule;
            }
            else if (decimal.TryParse(numericalValues[1], out decimal secondValueD))
            {
                if (itemsForPriceRule.Matches(rule).Count > 0)
                {
                    Rule<int, decimal> newRule = new Rule<int, decimal>
                    {
                        ruleType = RuleType.itemsForPrice,
                        FirstValue = FirstValue,
                        SecondValue = secondValueD
                    };

                    return newRule;
                }
            }

            return new Rule<int, int>();
        }

    }
}
