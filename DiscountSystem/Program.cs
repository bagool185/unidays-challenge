﻿using System;
using System.Collections.Generic;


namespace DiscountSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            var pricingRules = new List<(string ItemName, decimal Price, string DiscountRule)>
            {
                ( "A", 8.00m, "None" ),
                ( "B", 12.00m, "2 for £20.00" ),
                ( "C", 4.00m, "3 for £10.00" ),
                ( "D", 7.00m, "Buy 1 get 1 free" ),
                ( "E", 5.00m, "3 for the price of 2")
            };

            UnidaysDiscountChallenge test = new UnidaysDiscountChallenge(pricingRules);

            string basket = "EEE";

            foreach (char item in basket)
            {
                test.AddToBasket(item.ToString());
            }

            var (Total, DeliveryCharge) = test.CalculateTotalPrice();

            decimal totalPrice = Total;
            decimal deliveryCharge = DeliveryCharge;
            decimal overallTotal = totalPrice + deliveryCharge;

            Console.WriteLine($"Total price: {totalPrice}\n" +
                $"Delivery charge: {deliveryCharge}\n" +
                $"Overall total: {overallTotal}");

            Console.ReadKey();
        }
    }
}
