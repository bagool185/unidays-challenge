﻿### UnidaysDiscountChallenge

Discount system that allows its user to add items
to the basket and get the total price based on various
pricing rules.

#### Requirements

The project is meant to be used on a Windows 10 machine.

[.NET Framework 4.6.1](https://www.microsoft.com/en-gb/download/details.aspx?id=49981) or higher is required.        
The [System.ValueType](https://www.nuget.org/packages/System.ValueTuple/) NuGet package

#### Compilation

##### Visual Studio

1. Open the solution in Visual Studio

2. Build Solution (Build -> Build Solution or F7)

The executable will be created in `bin\Debug`

##### CLI

1. Open the Developer Command Prompt for VS
 in the same folder as the cs files

2. Write down ```csc UnidaysDiscountChallenge.cs Program.cs``` 

The executable will be created in the same directory

#### Behind the scenes

##### Data structures

Aside from the main class, the program uses the structure `Rule<T,U>` 

```C#
enum RuleType
{
	None,
	/* get N items of the same type for the price of X */
	itemsForPrice,
	/* buy item X, get another Y items of type X for free */
	buyXGetYFree,
	/* get X items for the price of Y items*/
	xForPriceY,
}

struct Rule<T, U>
{
    // Discount rule type
    public RuleType ruleType;
    // First numerical value in rule's text
    public T FirstValue { get; set; }
    // Second numerical value in rule's text
    public U SecondValue { get; set; }
}
```

##### Logic

The main class is initialised with the pricing rules in the form of ValueTupples.

Items' names are mapped to their prices and discount rules descriptions
contained within `PricingRules` and `DiscountRules` respectively.

In order to decide the 'discount rule', the text of the rule is matched against 
a series of RegEx patterns in the `ParseRule` method.

The numerical values within the rule text are retrieved using the `RetrieveNumericalValues` using RegEx.

Based on the rule type, a set of operations is applied to the numerical values retrieved in the previous step:

* Buy X items get Y for free 

	discount = price * number of same-type items / (X + Y)

* Get X items for the price of Y

	discount = price * number of same-type items / X

* Get X items for £Y

	discount = (price * X - Y) * number of same-type items / X


Finally, the deposit is subtracted from the total price and the delivery
charge is set accordingly.